let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];
let arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function moveArrToHtml(input){
  return input.map(el => {
    if(Array.isArray(el)) 
    return `<ul>${moveArrToHtml(el)}</ul>`;
    return `<li>${el}</li>`;
  }).join('');
}

function showOnPage(input, parent = document.body){
  parent.insertAdjacentHTML('beforeend', `<ul>${moveArrToHtml(input)}</ul>`);
}

function clearPage(time){
  let timer = document.createElement('p');
  timer.style.fontSize = '30px'
  timer.style.color = 'black';

  let interval = setInterval (() => {
    if (time > 0) {
      timer.innerText = time-- + 's';
      document.body.append(timer);
    }else{
      document.body.innerHTML = '';
      clearInterval(interval);
    }
  }, 1000)
}

showOnPage(arr1);
showOnPage(arr2);
showOnPage(arr3);
clearPage(3);
